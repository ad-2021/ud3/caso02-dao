package dao;

import java.sql.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.Cliente;

/**
 *
 * @author sergio
 */
public class ClienteDAOImpl implements GenericDAO<Cliente> {

	final String SQLSELECTALL = "SELECT * FROM clientes";
	final String SQLSELECTPK = "SELECT * FROM clientes WHERE id = ?";
	final String SQLINSERT = "INSERT INTO clientes (nombre, direccion) VALUES (?, ?)";
	final String SQLUPDATE = "UPDATE clientes SET nombre = ?, direccion = ? WHERE id = ?";
	final String SQLDELETE = "DELETE FROM clientes WHERE id = ?";
	private final PreparedStatement pstSelectPK;
	private final PreparedStatement pstSelectAll;
	private final PreparedStatement pstInsert;
	private final PreparedStatement pstUpdate;
	private final PreparedStatement pstDelete;

	public ClienteDAOImpl() throws SQLException {
		Connection con = ConexionBD.getConexion();
		pstSelectPK = con.prepareStatement(SQLSELECTPK);
		pstSelectAll = con.prepareStatement(SQLSELECTALL);
		pstInsert = con.prepareStatement(SQLINSERT);
		pstUpdate = con.prepareStatement(SQLUPDATE);
		pstDelete = con.prepareStatement(SQLDELETE);
	}
	
	public void cerrar() throws SQLException {
		pstSelectPK.close();
		pstSelectAll.close();
		pstInsert.close();
		pstUpdate.close();
		pstDelete.close();
	}

	public Cliente findByPK(int id) throws SQLException {
		Cliente c = null;
		pstSelectPK.setInt(1, id);
		ResultSet rs = pstSelectPK.executeQuery();
		if (rs.next()) {
			c = new Cliente(id, rs.getString("nombre"), rs.getString("direccion"));			
		}
		rs.close();
		return c;
	}

	public List<Cliente> findAll() throws SQLException {
		List<Cliente> listaClientes = new ArrayList<Cliente>();
		ResultSet rs = pstSelectAll.executeQuery();
		while (rs.next()) {
			listaClientes.add(new Cliente(rs.getInt("id"), rs.getString("nombre"), rs.getString("direccion")));
		}
		rs.close();
		return listaClientes;
	}

	public boolean insert(Cliente cliInsertar) throws SQLException {
		pstInsert.setString(1, cliInsertar.getNombre());
		pstInsert.setString(2, cliInsertar.getDireccion());
		int insertados = pstInsert.executeUpdate();
		return (insertados == 1);
	}

	public boolean update(Cliente cliActualizar) throws SQLException {
		pstUpdate.setString(1, cliActualizar.getNombre());
		pstUpdate.setString(2, cliActualizar.getDireccion());
		pstUpdate.setInt(3, cliActualizar.getId());
		int actualizados = pstUpdate.executeUpdate();
		return (actualizados == 1);
	}

	public boolean delete(int id) throws SQLException {
		pstDelete.setInt(1, id);
		int borrados = pstDelete.executeUpdate();
		return (borrados == 1);
	}

	public boolean delete(Cliente cliEliminar) throws SQLException {
		return this.delete(cliEliminar.getId());
	}
}
