package vista;

import java.sql.SQLException;
import java.util.List;

import dao.ClienteDAOImpl;
import dao.ConexionBD;
import modelo.Cliente;

public class App {

	public static void main(String[] args) {

		try {
			ClienteDAOImpl clidao = new ClienteDAOImpl();
			
			List<Cliente> clientes = clidao.findAll();
			for (Cliente cli : clientes) {
				System.out.println(cli);
			}
			Cliente cli = clidao.findByPK(4);
			if (cli != null) {
				System.out.println("Encontrado cli 4: " + cli);
			}

			// insertar
			Cliente cliInsertar = new Cliente("sergiodao", "direccion dao");
			if (clidao.insert(cliInsertar)) {
				System.out.println("Insertado!");
			}

			// actualizar
			Cliente cliModificar = clidao.findByPK(5);
			if (cliModificar != null) {
				cliModificar.setNombre("otro nombre");
				cliModificar.setDireccion("otra direccion");
				if (clidao.update(cliModificar)) {
					System.out.println("Cliente modificado!!");
				}
			}

			// eliminar
			if (clidao.delete(6)) {
				System.out.println("Cliente borrado!!");
			}
			Cliente cliEliminar = clidao.findByPK(7);
			if (cliEliminar != null) {
				if (clidao.delete(cliEliminar)) {
					System.out.println("Cliente borrado!!");
				}
			}
			
			clidao.cerrar();
			ConexionBD.cerrar();

		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}

	}

}
